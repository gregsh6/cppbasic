#include <iostream>

#include <string>
#include <clocale>
#include <ctime>
#include <math.h>
#include <stack>

using namespace std;

void SetLocateExample() {
    setlocale(LC_ALL, "Russian");
}

// ����������� ������� �����
int SquaredSum(int A, int B)
{
	return A * A + 2 * A * B + B * B;
}

void WorkWithTime()
{
    struct tm newtime;
    time_t now = time(0);
    localtime_s(&newtime, &now);
    std::cout << newtime.tm_mday;
}

void Foreach()
{
    int a[5] = { 1,2,3,4,5 };
    for (const auto& element : a)
    {
        std::cout << element;
    }
}

class Vector
{
private:
    double x = 0;
    double y = 0;
    double z = 0;
public:
    Vector()
    {}
    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}

    void Print()
    {
        cout << "\n������: " << x << " " << y << " " << z;
    }

    double Length()
    {
        return sqrt(x * x + y * y + z * z);
    }
};

class MyStack
{
private:
    int Top = 0;
    int* stack = nullptr;
public:
    ~MyStack()
    {
        delete[] stack;
    }

    void Push(int item) {
        int* tmp = stack;
        stack = new int[Top + 1];
        Top++;
        for (int i = 0; i < Top - 1; i++)
        {
            stack[i] = tmp[i];
        }

        stack[Top - 1] = item;

        delete[] tmp;
    }

    int Pop() {
        if (Top <= 0)
        {
            return 0;
        }

        Top--;
        return stack[Top];
    }
};

class Animal
{
public:
    virtual ~Animal()
    {

    }
    virtual void Voice()
    {
        cout << "default voice";
    }
};

class Dog : public Animal
{
public:
    void Voice() override
    {
        cout << "Woof!";
    }
};

class Cat : public Animal
{
public:
    void Voice() override
    {
        cout << "Myyauuu!";
    }
};

class Cow : public Animal
{
public:
    void Voice() override
    {
        cout << "Muuuu!";
    }
};

enum class Result
{
	SUCCESS,
	FAIL,
	ERROR_WRITE,
	ERROR_READ
};

enum class Another
{
	WORK,
	SUCCESS
};

Result DoWork()
{
	return Result::ERROR_WRITE;
}

enum Alphabet
{
	A,
	B,
	C
};

struct Bag
{
	string books[];
};
struct Student
{
	int Age = 0;
	int Height = 0;
	string Name = "";
	Bag* MyBag = nullptr;

	void GetInfo()
	{
		cout << "student struct" << "\n";
	}

	~Student()
	{
		delete[] MyBag;
		MyBag = nullptr;
	}
};

void EnumAndStructWork() {
	//Enum work
	cout << A << "\n";
	Result workResult = DoWork();
	cout << static_cast<int>(workResult) << "\n";
	static_cast<Result>(3);
	if (workResult == Result::SUCCESS)
	{

	}

	Another anotherResult = Another::WORK;
	if (static_cast<int>(workResult) == static_cast<int>(anotherResult))
	{

	}

	//Struct work
	Student* ptr = new Student{ 33, 194, "Greg" };
	ptr->GetInfo();
	delete ptr;
}