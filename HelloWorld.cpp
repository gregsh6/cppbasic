#include <iostream>
using namespace std;

class IA
{
	public:
		IA()
		{
			cout << "class IA constructed\n";
		}
		virtual ~IA()
		{
		}
		virtual void ShowInfo() = 0;
};

void IA::ShowInfo()
{
	cout << "default implementation\n";
}

class B : virtual public IA
{
public:
	B()
	{
		cout << "class B constructed\n";
	}
	~B()
	{
	}
	virtual void ShowInfo() override
	{
		IA::ShowInfo();
		cout << "Show Info in class B\n";
	}
};

class C : virtual public IA
{
public:
	C()
	{
		cout << "class C constructed\n";
	}
	~C()
	{
	}
	virtual void ShowInfo() override
	{
		IA::ShowInfo();
	}
};

class D : virtual public B, virtual public C
{
public:
	D()
	{
		cout << "class D constructed\n";
	}
	virtual void ShowInfo() override
	{
		IA::ShowInfo();
	}
};

int main()
{
	IA* a = new B;
	a->ShowInfo();
	delete a;
	
	cout << "\n";

	D* b = new D;
	delete b;
}